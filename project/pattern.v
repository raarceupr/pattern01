`timescale 1ns / 1ps
// Code your multiplier design

module pattern(clk, rst, leds);
  localparam state_A = 0, state_B = 1, state_C = 2, state_D = 3;
  
  input clk, rst;
  output reg [3:0] leds = 4'b0000;
  
  reg [30:0] cnt = 30'b0;
  reg [1:0] state = state_A;
  
  
  always @(posedge clk) begin
    if (rst) begin
      state <= state_A;
      leds <= 4'b0100;
      cnt <= 0;
    end else begin
      state <= state;
      cnt <= cnt + 1;
      case (state)
        state_A: begin
          leds <= 4'b0100;
          if (cnt[24]==1) begin
          	state <= state_B;
            cnt <= 0;
          end
        end
        state_B: begin
          leds <= 4'b0010;
          if (cnt[25]==1) begin
          	state <= state_C;
            cnt <= 0;
          end
        end
		  
        state_C: begin
          leds <= 4'b1000;
          if (cnt[24]==1) begin
          	state <= state_D;
            cnt <= 0;
          end
        end
		  
        state_D: begin
          leds <= 4'b0001;
          if (cnt[24]==1) begin
          	state <= state_A;
            cnt <= 0;
          end
        end


      endcase
    end
    
  end
  
  
endmodule

