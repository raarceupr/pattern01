`timescale 1ns / 1ps
// Testbench for 4-bit counter

module pattern_tb();

   
  reg clk, rst;
  wire [3:0] leds;  
  
  pattern DUT (.clk(clk), .rst(rst), .leds(leds) );
  
  initial
  begin
    //$monitor($time, " clk=%b, rst=%b, q=%b", rst, clk, q);
    $dumpfile("dump.vcd");
    $dumpvars(1, DUT);
  end  
  
  always begin
    #5 clk = ~clk;
  end
  
  initial begin
    clk = 1'b0; rst = 1'b0; 
    #22 rst = 1'b1; 
    #12 rst = 1'b0;
    


    @(posedge clk);
    @(posedge clk); @(posedge clk);  @(posedge clk);  @(posedge clk);
    @(posedge clk);  @(posedge clk);  @(posedge clk);  @(posedge clk);
    @(posedge clk);  @(posedge clk);

  end
   
endmodule

