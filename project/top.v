// Module to receive USART letters from the PC and toggle
// 4 leds.
// Rafael Arce Nazario 2017

module top
(
    input wire          clk,
//    input wire          ftdi_rx,
	 output  [3:0]  	led 
);

pattern pattern00 (.clk(clk), .rst(1'b0), .leds(led));




endmodule
